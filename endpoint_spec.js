var frisby = require('frisby');

var URL = 'http://10.0.12.147:8000';
var queuePath = '/work-orders';
var useAbsolute = true;

// var URL = 'http://10.0.12.137:1234';
// var queuePath = '/math/input-queue';
// var useAbsolute = false;


frisby.create('read queue')
  .get(URL + queuePath)
  .expectStatus(200)
  .expectJSONTypes('collection.items.*', {
    href: String,
  })
  //.inspectJSON()
  .afterJSON(function(body) {
    if (body.collection.items.length > 0) {
      workorder = body.collection.items[0];

      frisby.create('read work item')
        .addHeader('Accept', 'application/vnd.mogsie.work-order+json')
        .get((useAbsolute ? '' : URL) + workorder.href)
        .expectStatus(200)
        .expectHeader('Content-Type', 'application/vnd.mogsie.work-order+json')
        .expectJSONTypes({
          type: String,
          input: function(val) { expect(val).toBeTypeOrNull(Object); },
          start: String,
          status: String,
          complete: String,
          fail: String
        })
        .afterJSON(function(item) {
          frisby.create('take item')
            .post((useAbsolute ? '' : URL) + item.start)
            .expectStatus(200)
            .expectJSONTypes({
              input: function(val) { expect(val).toBeTypeOrNull(Object); },
              type: String
            })
            .afterJSON(function() {
              frisby.create('check status after take')
                .get((useAbsolute ? '' : URL) + item.status)
                .expectStatus(200)
                .expectJSON('status', {
                  state: 'started'
                })
              .toss();
              frisby.create('fail item')
                .post((useAbsolute ? '' : URL) + item.fail)
                .after(function () {
                  frisby.create('check status after fail')
                    .get((useAbsolute ? '' : URL) +  item.status)
                    .expectJSON('status', {
                      state: 'failed'
                    })
                  .toss();
                })
              .toss();
            })
          .toss();
        })
      .toss();      
    }
  })
.toss();